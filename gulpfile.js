const debug = process.env.NODE_ENV !== 'production';
console.log('*************************************');
console.log('************* '+ (debug ? 'Debug' : 'Production') +' *************');
console.log('*************************************');

// gulp 4

const { series, parallel, src, dest, watch } = require('gulp');
const gulpif = require('gulp-if');
const ugly = require('gulp-uglify');
const clean = require('gulp-clean-css');
const concat = require('gulp-concat');
const less = require('gulp-less');

// CORE

function coreCss() {
    return src([
        // bootstrap
        'resources/vendor/bootstrap-sandstone.css',

        // fontawesome
        'node_modules/font-awesome/css/font-awesome.css',

        // material design icons
        'resources/vendor/material-design-iconic-font/css/material-design-iconic-font.css',

        // animate.css
        'node_modules/animate.css/animate.css',

        // datetimepicker
        // 'resources/vendor/tempusdominus-bootstrap-4.min.css',

        // modal
        'node_modules/rmodal/dist/rmodal.css',
    ])
        .pipe(concat('core.min.css'))
        .pipe(clean({compatibility: 'ie8'}))
        .pipe(dest('public_html/css/'))
}

function coreJs() {
    return src([
        // jquery
        'node_modules/jquery/dist/jquery.js',

        // popper (bootstrap 4 related)
        'node_modules/popper.js/dist/umd/popper.js',

        // bootstrap
        'node_modules/bootstrap/dist/js/bootstrap.js',

        // moment
        'node_modules/moment/min/moment-with-locales.js',

        // datetimepicker
        // 'resources/vendor/tempusdominus-bootstrap-4.min.js',
    ])
        .pipe(concat('core.min.js'))
        .pipe(ugly())
        .pipe(dest('public_html/js'));
}

function coreWelcomeJs() {
    return src([
        'resources/vendor/parallax.js-1.5.0/parallax.min.js',
        'node_modules/scrollreveal/dist/scrollreveal.js',
    ])
        .pipe(concat('core-welcome.min.js'))
        .pipe(ugly())
        .pipe(dest('public_html/js'));
}

// main

function css() {
    return src('resources/less/main.less')
        .pipe(concat('main.min.less'))
        .pipe(less())
        .pipe(gulpif(!debug, clean({compatibility: 'ie8'})))
        .pipe(dest('public_html/css'))
}

function js() {
    return src('resources/js/main.js')
        .pipe(concat('main.min.js'))
        .pipe(gulpif(!debug, ugly()))
        .pipe(dest('public_html/js'))
}

// watchers

function watchCss() {
    if (debug) {
        return watch('resources/less/**/*.less', { ignoreInitial: false }, css)
    }
    return css();
}

function watchJs() {
    if (debug) {
        return watch('resources/js/main.js', { ignoreInitial: false }, js)
    }
    return js();
}

// exports

exports.core = parallel(coreCss, coreWelcomeJs, coreJs);
exports.main = parallel(css, js);
exports.default = series(coreCss, coreJs, coreWelcomeJs, parallel(watchCss, watchJs));