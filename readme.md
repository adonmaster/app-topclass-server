## TOPCLASS

```
mkdir storage
mkdir storage/app
mkdir storage/app/attachments
mkdir storage/framework
mkdir storage/framework/sessions
mkdir storage/framework/views
```

```
// linux
ln -s ../storage/app storage
```
- windows _(public folder && run as admin)_
```
// windows
mklink /D storage ..\storage\app
```

- Problemas de memory_limit running composer no linux
```
php -d memory_limit=4096M ~/bin/composer/composer.phar update -vvv
```

- hostinger composer
```
php -d memory_limit=4096M /usr/local/bin/composer.phar update -vvv
```

- use composer.lock para deploy! e composer install (ao inves de update)
```
scp composer.lock adon:domains/syncer.com.br/
```

- Mocha-webpack, se der problema no --watch, pegue a versão 2.beta

- and done!

## config hostgator
-- .ssh/authorized_keys:
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDkSr4yREBKSPM3iolDw6V+HYiUeqG7FazHLZvAGGsVVk54fIS9NIi+lErmg9/OgmsL442ZzfgsojgNafpOd9dBk6q6vQDoNBfqD2EKCLVOqD0NpR3K0H6ABaU0F1TsHH66efO/efw+Y2IveBRWtRfmCgdtsAupJcFAb1KSlVpu1eU9zpcnT1rbmfTavXyySl/R7rfucFaiSLsQ4dXNF+gqjGb549iec/GMVu7zrm7UDWbZTKK1DlrEKBN3am3Hoq73aboFkd6w2tRFLyq+b9OGzVweiW7tu5WoZDwTIMAu4uLqrK2HyyfICvJ93KtRtcxTOE4/fbYCJxx/5F520u3X Adon@ADON10

* .bashrc
```
#pra que essa merda? nao sei..mas falaram pra fazer isso e composer iria funfar :\
[ -z "$PS1" ] && return

# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# User specific aliases and functions

alias php='/opt/php71/bin/php'
alias composer='~/bin/composer/composer.phar'

export PATH="/opt/php71/bin:$PATH"
```