<?php

use Illuminate\Http\Request;

\Route::post('register', 'ApiRegisterController@register');
\Route::post('register/code', 'ApiRegisterController@code');

# Profile
Route::group(['middleware' => 'auth:api'], function() {

    // ping
    Route::get('/', function() {
        return ['app' => 'TopClass'];
    });

    // profile
    Route::get('profile', 'ApiProfileController@index');
    Route::post('profile', 'ApiProfileController@store');

    // profile card
    Route::get('profile/card', 'ApiProfileCardController@index');
    Route::post('profile/card', 'ApiProfileCardController@store');

    // pro requisition
    Route::post('pro_requisition', 'ApiProRequisitionController@store');
    Route::get('pro_requisition', 'ApiProRequisitionController@index');

    // pro skills
    Route::get('skills', 'ApiSkillController@index');
    Route::post("skills", 'ApiSkillController@store');

    // pros
    Route::get('pro/query', 'ApiProController@query');

    // gcm
    Route::post('gcm', 'ApiGcmController@store');

    // proposals
    Route::post('proposals', 'ApiProposalController@store');
    Route::get('proposals', 'ApiProposalController@index');
    Route::post('proposals/card-token', 'ApiProposalController@cardToken');
    Route::post('proposals/accept', 'ApiProposalController@accept');
    Route::delete('proposal/{proposal}', 'ApiProposalController@destroy');

    // contracts
    Route::get('contracts','ApiContractController@index');

    // transactions
    Route::get('transactions', 'ApiTransactionController@index');

    // ads
    Route::get('ads', 'ApiAdController@index');
    Route::get('ad/{ad}', 'ApiAdController@find');
    Route::post('ads', 'ApiAdController@store');
    Route::post('ads/{ad}/subscribe', 'ApiAdController@subscribe');
    Route::post('ads/{ad}/unsubscribe', 'ApiAdController@unsubscribe');
    Route::delete('ads/{ad}', 'ApiAdController@destroy');

});

Route::get('test', function() {

    $envStr = getenv('APN_IS_SANDBOX') == 'true' ? 'SANDBOX' : 'PROD';

    \App\Adon\Gcm\Gcm::to(\App\User::all())->send(
        new \App\Adon\Gcm\Bodies\GcmBasicBody(
            "Mensagem $envStr",
            'ConecteTest',
            ['reason' => 'proposal_update']
        )
    );

    return 'done';

});