<?php

Route::get('/', function () {
    return view('welcome');
});

// session
Route::get('login', 'SessionController@login')->name('login');
Route::post('login', 'SessionController@storeLogin')->name('login');
Route::delete('login', 'SessionController@logoff')->name('login');

// admin
Route::group(['prefix' => 'admin', 'middleware' => 'auth.admin'], function() {

    // index
    Route::get('/', 'AdminController@index')->name('admin.index');

    // pro requisition
    Route::get('/pro-requisition', 'AdminController@proRequisition')
        ->name('admin.index-pro-requisition');
    Route::delete('pro-requisition', 'AdminController@proRequisitionDestroy');
    Route::post('pro-requisition', 'AdminController@proRequisitionConfirm');

});


// test
Route::get('test', function() {

//    (new \App\Http\Controllers\ApiProposalController())->

});
