<?php

use App\Skill;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    public function run()
    {
        if (Skill::count()) return;

        $descs = [
            "Língua Portuguesa/Fundamental 1",
            "Língua Portuguesa/Fundamental 2",
            "Língua Portuguesa/Médio",
            "Língua Portuguesa/Superior",
            "Língua Portuguesa/Concurso",
            "Matemática/Fundamental 1",
            "Matemática/Fundamental 2",
            "Matemática/Médio",
            "Matemática/Superior",
            "Matemática/Concurso",
            "Ciências /Fundamental 1",
            "Ciências /Fundamental 2",
            "Ciências /Médio",
            "Ciências /Superior",
            "Ciências /Concurso",
            "Física/Fundamental 1",
            "Física/Fundamental 2",
            "Física/Médio",
            "Física/Superior",
            "Física/Concurso",
            "Química/Fundamental 1",
            "Química/Fundamental 2",
            "Química/Médio",
            "Química/Superior",
            "Química/Concurso",
            "Biologia/Fundamental 1",
            "Biologia/Fundamental 2",
            "Biologia/Médio",
            "Biologia/Superior",
            "Biologia/Concurso",
            "Geografia/Fundamental 1",
            "Geografia/Fundamental 2",
            "Geografia/Médio",
            "Geografia/Superior",
            "Geografia/Concurso",
            "História/Fundamental 1",
            "História/Fundamental 2",
            "História/Médio",
            "História/Superior",
            "História/Concurso",
            "Inglês/Fundamental 1",
            "Inglês/Fundamental 2",
            "Inglês/Médio",
            "Inglês/Superior",
            "Inglês/Concurso",
            "Filosofia/Fundamental 1",
            "Filosofia/Fundamental 2",
            "Filosofia/Médio",
            "Filosofia/Superior",
            "Filosofia/Concurso"
        ];

        foreach ($descs as $desc) {
            Skill::create(compact('desc'));
        }
    }
}
