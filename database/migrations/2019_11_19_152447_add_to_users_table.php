<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('gcm_token')->nullable()->after('last_active');
            $table->string('gcm')->nullable()->after('last_active');
            $table->boolean('is_pro')->default(false)->after('last_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_pro');
            $table->dropColumn('gcm');
            $table->dropColumn('gcm_token');
        });
    }
}
