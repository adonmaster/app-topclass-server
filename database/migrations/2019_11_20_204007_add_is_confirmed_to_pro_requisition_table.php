<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsConfirmedToProRequisitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pro_requisition', function (Blueprint $table) {
            $table->boolean('is_resolved')->default(false)->after('obs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pro_requisition', function (Blueprint $table) {
            $table->dropColumn('is_resolved');
        });
    }
}
