<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProposalsTableV1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposals', function(Blueprint $bp) {
            $bp->string('order_customer_card_token', 2000)->after('price');
            $bp->string('order_customer_name', 255)->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function(Blueprint $bp) {
            $bp->dropColumn(['order_customer_card_token', 'order_customer_name']);
        });
    }
}
