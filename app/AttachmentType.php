<?php namespace App;


use Illuminate\Http\UploadedFile;

interface AttachmentType
{
    /**
     * @return string
     */
    public function desc();

    /**
     * @param UploadedFile $file
     * @return AttachmentTypeResult
     */
    public function saveFile(UploadedFile $file);

}