<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 08-Apr-20
 * Time: 20:01
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


/**
 * Class AdPro
 *
 * @package App
 * @property int $id
 * @property int $ad_id
 * @property int $pro_user_id
 * @property float $price
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @mixin \Eloquent
 * @property-read \App\User $proUser
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro whereAdId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro whereProUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AdPro whereUpdatedAt($value)
 */
class AdPro extends Model
{
    protected $table = 'ad_pro';
    protected $fillable = [
        'ad_id', 'pro_user_id', 'price'
    ];
    protected $hidden = [];
    protected $dates = [];
    public $timestamps = true;
    protected $casts = [
        'price' => 'float'
    ];
    protected $with = ['proUser'];


    # Relationships

    public function proUser()
    {
        return $this->belongsTo(User::class, 'pro_user_id', 'id');
    }

}