<?php namespace App;


use Illuminate\Http\UploadedFile;

class AttachmentTypeAny implements AttachmentType
{

    /**
     * @return string
     */
    public function desc()
    {
        return 'any';
    }

    public function saveFile(UploadedFile $file)
    {
        $attachmentDirectory = public_path("/storage/attachments");
        if ( ! \File::exists($attachmentDirectory)) \File::makeDirectory($attachmentDirectory);

        $ext = $file->getClientOriginalExtension() ?: 'dat';
        $filename = time() .'_'. str_random(16) . '.' . $ext;

        $file->storeAs("attachments", $filename);

        $path = public_path("/storage/attachments/$filename");

        return new AttachmentTypeResult(
            'local',
            $path,
            null,
            asset("storage/attachments/$filename"),
            null
        );
    }


}