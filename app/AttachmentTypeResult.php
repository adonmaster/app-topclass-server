<?php namespace App;


/**
 * Class AttachmentTypeResult
 * @package App
 */
class AttachmentTypeResult
{
    public $server;
    public $path;
    public $path_thumb;
    public $url;
    public $url_thumb;

    /**
     * AttachmentTypeResult constructor.
     * @param $server
     * @param $path
     * @param $path_thumb
     * @param $url
     * @param $url_thumb
     */
    public function __construct($server, $path, $path_thumb, $url, $url_thumb)
    {
        $this->server = $server;
        $this->path = $path;
        $this->path_thumb = $path_thumb;
        $this->url = $url;
        $this->url_thumb = $url_thumb;
    }


}