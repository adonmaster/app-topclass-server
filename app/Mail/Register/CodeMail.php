<?php

namespace App\Mail\Register;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CodeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $code;
    public $email;

    /**
     * CodeMail constructor.
     * @param $code
     * @param $email
     */
    public function __construct($email, $code)
    {
        $this->code = $code;
        $this->email = $email;
    }

    public function build()
    {
        return $this
            ->to($this->email)
            ->subject('Confirmação de registro')
            ->view('mails.user-register-send-code');
    }
}
