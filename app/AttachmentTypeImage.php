<?php namespace App;


use Illuminate\Http\UploadedFile;

class AttachmentTypeImage implements AttachmentType
{

    /**
     * @return string
     */
    public function desc()
    {
        return 'image';
    }

    public function saveFile(UploadedFile $file)
    {
        $attachmentDirectory = public_path("storage/attachments");
        if ( ! \File::exists($attachmentDirectory)) \File::makeDirectory($attachmentDirectory);

        // original
        $ext = $file->getClientOriginalExtension() ?: 'png';
        $filename = time() .'_'. str_random(12) . '.' . $ext;
        $pathFull = public_path("storage/attachments/$filename");

        \Image::make($file)->resize(1024, null, function($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save($pathFull);

        // thumb
        $filenameThumb = time() .'_'. str_random(12) . '.thumb.' . $ext;
        $pathThumb = public_path("storage/attachments/$filenameThumb");
        \Image::make($file)->resize(150, null, function($c) {
            $c->aspectRatio();
            $c->upsize();
        })->save($pathThumb);

        return new AttachmentTypeResult(
            'local',
            $pathFull,
            $pathThumb,
            asset("storage/attachments/$filename"),
            asset("storage/attachments/$filenameThumb")
        );
    }


}