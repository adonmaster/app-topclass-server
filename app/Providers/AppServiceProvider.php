<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Carbon\Carbon;
use Clockwork\Support\Laravel\ClockworkServiceProvider;
use Illuminate\Support\ServiceProvider;
use Laracasts\Generators\GeneratorsServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \Schema::defaultStringLength(191);

        // Carbon internationalization
        Carbon::setLocale('pt-BR');
        setlocale(LC_TIME, 'pt-BR');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('path.public_html', function() {
            return base_path().'/public_html';
        });

        $this->app->bind('path.public', function() {
            return base_path().'/public_html';
        });

        if ($this->app->environment() !== 'production')
        {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(GeneratorsServiceProvider::class);
            $this->app->register(ClockworkServiceProvider::class);
        }
    }
}
