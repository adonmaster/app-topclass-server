<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Proposal
 *
 * @package App
 * @property int $id
 * @property int $client_user_id
 * @property int $pro_user_id
 * @property [] $body
 * @property string $order_customer_card_token
 * @property string $order_customer_name
 * @property double $price
 * @property Carbon $due_at
 * @property int $phase
 * @property Carbon $expire_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read User $client_user
 * @property-read User $pro_user
 * @mixin \Eloquent
 * @property-read \App\User $clientUser
 * @property-read \App\User $proUser
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereClientUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereDueAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereExpireAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereOrderCustomerCardToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereOrderCustomerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal wherePhase($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereProUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Proposal whereUpdatedAt($value)
 */
class Proposal extends Model
{
    //
    const PHASE_CONCLUDED = 3;

    //
    protected $table = 'proposals';
    protected $fillable = [
        'client_user_id', 'pro_user_id', 'body', 'price', 'phase',
        'order_customer_card_token', 'order_customer_name', 'expire_at', 'due_at'
    ];
    protected $hidden = [];
    protected $dates = ['expire_at'];
    public $timestamps = true;
    protected $casts = ['body' => 'array'];
    protected $with = ['clientUser', 'proUser'];


    # Relationships

    public function clientUser()
    {
        return $this->belongsTo(User::class, 'client_user_id', 'id');
    }

    public function proUser()
    {
        return $this->belongsTo(User::class, 'pro_user_id', 'id');
    }

}
