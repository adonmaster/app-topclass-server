<?php

namespace App;

use App\Adon\Presenters\UserPresenter;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * App\User
 *
 * @property int id
 * @property boolean $is_admin
 * @property $name
 * @property $email
 * @property $password
 * @property $api_token
 * @property $avatar
 * @property $avatar_thumb
 * @property $is_pro
 * @property $gcm
 * @property $gcm_token
 * @property Carbon $last_active
 * @property $email_verified_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @mixin \Eloquent
 * @property string|null $remember_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attachment[] $attachments
 * @property-read int|null $attachments_count
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereApiToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAvatarThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGcm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGcmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsPro($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @property int $id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Skill[] $skills
 * @property-read int|null $skills_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User pro()
 */
class User extends Authenticatable
{
    use Notifiable;

    // static
    protected static function boot()
    {
        parent::boot();

        static::saving(function($model) {
            $model->last_active = Carbon::now();
        });
    }

    // fields
    protected $fillable = [
        'is_admin', 'name', 'email', 'password', 'api_token', 'password', 'email_verified_at',
        'avatar', 'avatar_thumb', 'last_active', 'is_pro', 'gcm', 'gcm_token'
    ];
    protected $hidden = [
        'password', 'remember_token', 'api_token', 'gcm', 'gcm_token', 'is_admin'
    ];
    protected $dates = ['last_active'];
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_pro' => 'boolean',
        'is_admin' => 'boolean'
    ];

    // presenter

    private $presenterInstance = null;
    /**
     * @return UserPresenter
     */
    public function p()
    {
        if ( ! $this->presenterInstance) $this->presenterInstance = new UserPresenter($this);
        return $this->presenterInstance;
    }

    // scopes
    public function scopePro($q)
    {
        $q->where('is_pro', true);
    }


    // Mutators and Accessors

    public function setPasswordAttribute($v)
    {
        $this->attributes['password'] = \Hash::make($v);
    }


    # Relationships

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    public function skills()
    {
        return $this->belongsToMany(
            Skill::class, 'pro_skill', 'user_id', 'skill_id',
            'id', 'id'
            )->withPivot(['comp', 'option'])
            ->where('option', true);
    }


    # Getters

    /**
     * @return bool
     */
    public function isGcmFcmService()
    {
        return $this->attributes['gcm'] == 'fcm';
    }

    /**
     * @return bool
     */
    public function isGcmApnService()
    {
        return $this->attributes['gcm'] == 'apn';
    }

    /**
     * @return string
     */
    public function getGcmToken()
    {
        return $this->attributes['gcm_token'];
    }

}
