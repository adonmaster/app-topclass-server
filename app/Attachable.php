<?php namespace App;


use Illuminate\Database\Eloquent\Relations\MorphMany;

interface Attachable
{
    /**
     * @return MorphMany
     */
    public function attachments();

}