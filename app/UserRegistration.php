<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\UserRegistration
 *
 * @property int $id
 * @property $user_id
 * @property $code
 * @property Carbon $expired_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserRegistration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserRegistration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserRegistration query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserRegistration whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserRegistration whereExpiredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserRegistration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserRegistration whereUserId($value)
 */
class UserRegistration extends Model
{
    protected $table = 'user_registration';
    protected $fillable = ['user_id', 'code', 'expired_at'];
    protected $hidden = [];
    protected $dates = ['expired_at'];
    public $timestamps = false;
    protected $casts = [];
    protected $with = [];
}
