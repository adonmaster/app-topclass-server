<?php namespace App\Jobs\Proposals;

use App\Jobs\Job;

abstract class CreateJob extends Job {

	protected $_pro_user_id;
	protected $_body;
	protected $_customer_name;
	protected $_card_token;
    protected $_client_user_id;
    protected $_price;
    protected $_due_at;

    public function __construct($data, $client_user_id)
    {
        extract($data);

		$this->_pro_user_id = $pro_user_id;
		$this->_client_user_id = $client_user_id;
		$this->_body = $body;
		$this->_customer_name = $customer_name;
		$this->_card_token = $card_token;
		$this->_price = $price;
		$this->_due_at = $due_at;
    }

}
