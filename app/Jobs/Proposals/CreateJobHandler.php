<?php namespace App\Jobs\Proposals;


use App\Adon\Gcm\Bodies\GcmBasicBody;
use App\Adon\Gcm\Gcm;
use App\Adon\Repo\Repo;
use Carbon\Carbon;

class CreateJobHandler extends CreateJob {

    /**
     * Do your thing!
     *
     */
    public function handle()
    {
        // model
        $phase = 1;
        $expire_at = Carbon::now()->addHours(48);

        $model = Repo::proposal()->create(
            $this->_client_user_id, $this->_pro_user_id, $this->_body,
            $this->_price, $phase, $this->_card_token, $this->_customer_name, $expire_at,
            $this->_due_at
        );

        // notify pro
        $client = Repo::user()->find($this->_client_user_id);
        $message = "{$client->name} tem uma proposta para você.";
        $title = "Proposta de aula!";
        $payload = ['reason' => 'proposal_new'];

        Gcm::to($this->_pro_user_id)->send(new GcmBasicBody($message, $title, $payload));

        //
        return $model;
    }
}
