<?php namespace App\Jobs;


use App\Adon\Commander\CommanderJobInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

abstract class Job implements ShouldQueue, CommanderJobInterface
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
}