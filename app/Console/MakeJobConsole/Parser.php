<?php namespace App\Console\MakeJobConsole;


class Parser {

    const BASE_PATH = 'app/Jobs/';

    /**
     * @param string $className
     * @param string $fields
     *
     * @return \App\Console\MakeJobConsole\Input
     */
    public function parse($className, $fields)
    {
        $segments = explode('\\', str_replace('/', '\\', $className));

        $name = array_pop($segments);
        $namespace = implode('\\', $segments);
        $fields = $this->parseFields($fields);
        $filePath = $this->parseFilePath($segments, $name);
        $nameHandler = "{$name}Handler";
        $handlerFilePath = $this->parseFilePath($segments, $nameHandler);

        return new Input($name, $namespace, $fields, $filePath, $nameHandler, $handlerFilePath);
    }

    private function parseFields($fields)
    {
        return preg_split('/ ?[,;] ?/', $fields, null, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * @param string[] $segments
     * @param $name
     *
     * @return string
     */
    private function parseFilePath($segments, $name)
    {
        $suffix = '';
        if (count($segments)) $suffix = implode('/', $segments) . '/';
        $suffix .= "$name.php";

        return self::BASE_PATH . $suffix;
    }

}