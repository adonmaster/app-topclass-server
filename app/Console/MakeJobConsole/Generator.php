<?php namespace App\Console\MakeJobConsole;


class Generator {

    const TEMPLATE_NAMESPACE = '{{__NAMESPACE__}}';
    const TEMPLATE_NAME = '{{__NAME__}}';
    const TEMPLATE_NAME_HANDLER = '{{__NAME_HANDLER__}}';
    const TEMPLATE_FIELDS_DECLARATION = '{{__FIELDS_DECLARATION__}}';
    const TEMPLATE_FIELDS_EXTRACTION = '{{__FIELDS_EXTRACTION__}}';

    public function generate(Input $input, $job_template, $handler_template)
    {
        $this->renderJob($input, $job_template);
        $this->renderHandler($input, $handler_template);
    }

    /**
     * @param Input $input
     * @param $job_template
     */
    public function renderJob(Input $input, $job_template)
    {
        $content = file_get_contents($job_template);

        $body = $this->renderNamespace($content, $input->namespace);
        $body = $this->renderName($body, $input->name);
        $body = $this->renderFields($body, $input->fields);

        $this->createFile($input->file_path, $body);
    }

    /**
     * @param Input $input
     * @param string $handler_template
     */
    private function renderHandler($input, $handler_template)
    {
        $content = file_get_contents($handler_template);

        $body = $this->renderNamespace($content, $input->namespace);
        $body = $this->renderName($body, $input->name);
        $body = $this->renderNameHandler($body, $input->name_handler);

        $this->createFile($input->handler_path, $body);
    }

    private function renderNamespace($content, $namespace)
    {
        $s = $namespace ? "\\$namespace" : '';
        return str_replace(self::TEMPLATE_NAMESPACE, $s, $content);
    }

    private function renderName($content, $name)
    {
        return str_replace(self::TEMPLATE_NAME, $name, $content);
    }

    private function renderNameHandler($content, $name)
    {
        return str_replace(self::TEMPLATE_NAME_HANDLER, $name, $content);
    }

    private function renderFields($content, $fields)
    {
        $declarationBody = '';
        $extractionBody = '';

        foreach ($fields as $i => $f)
        {
            if ($i)
            {
                $declarationBody .= "\n";
                $extractionBody .= "\n";
            }

            $declarationBody .= "\tprotected \$_$f;";
            $extractionBody .= "\t\t\$this->_$f = \$$f;";
        }

        $r = str_replace(self::TEMPLATE_FIELDS_DECLARATION, $declarationBody, $content);
        return str_replace(self::TEMPLATE_FIELDS_EXTRACTION, $extractionBody, $r);
    }

    private function createFile($file_path, $body)
    {
        $this->createDirForFile($file_path);

        file_put_contents($file_path, $body);
    }

    /**
     * @param string
     */
    public function createDirForFile($file)
    {
        $dir = dirname($file);

        if ( ! file_exists($dir)) mkdir($dir, '0777', true);
    }
}