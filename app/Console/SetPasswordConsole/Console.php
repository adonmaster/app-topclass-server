<?php namespace App\Console\SetPasswordConsole;


use App\User;
use Illuminate\Console\Command;

class Console extends Command
{
    protected $signature = 'adon:user-password {email} {password} {admin}';

    protected $description = 'Num inventa!';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $password = $this->argument('password');
        $admin = $this->argument('admin');

        $user = User::where(compact('email'))->first();
        if ( ! $user) {
            $this->error('Não existe esse usuário?!');
            return;
        }

        // moving on
        $user->password = $password;
        if ($admin=='true') $user->is_admin = true;
        if ($admin=='false') $user->is_admin = false;
        $user->save();

        $this->info('Prontinho... vai trampar!');
    }
}