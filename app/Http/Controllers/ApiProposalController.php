<?php

namespace App\Http\Controllers;

use App\Adon\Gcm\Bodies\GcmBasicBody;
use App\Adon\Gcm\Gcm;
use App\Adon\Repo\Repo;
use App\Jobs\Proposals\CreateJobHandler;
use App\Jobs\Proposals\UpdateJobHandler;
use App\Proposal;

class ApiProposalController extends Controller
{

    public function index()
    {
        $models = Repo::proposal()->getRelated(\Auth::id());

        return $this->responseOk($models);
    }

    public function store()
    {
        $data = request()->validate([
            'pro_user_id' => 'required',
            'body' => 'required',
            'customer_name' => 'required',
            'card_token' => 'required',
            'price' => 'required',
            'due_at' => 'required'
        ]);

        $proposal = $this->execute(new CreateJobHandler($data, \Auth::id()));

        return $this->responseOk(compact('proposal'));
    }

    public function cardToken()
    {
        $data = request()->validate([
            'no' => 'required',
            'valid_month' => 'required',
            'valid_year' => 'required',
            'secret' => 'required',
            'name' => 'required'
        ]);

        // do some crazy shit

        $token = 'TOKEN_CARD_'. time();

        return $this->responseOk(compact('token'));
    }

    public function accept()
    {
        $data = request()->validate([
            'id' => 'required'
        ]);

        $proposalModel = Repo::proposal()->find($data['id']);

        // credit card transaction
        sleep(random_int(2, 4));
        $fees = 2.4;

        // create contract model
        $contractModel = Repo::contract()->createFrom($proposalModel);

        // save the damn transaction table
        Repo::transaction()->create(
            $proposalModel->client_user_id,
            'credit',
            get_class($contractModel), $contractModel->id,
            $proposalModel->price,
            "Pagamento relativo ao contrato #" . $contractModel->id
        );
        Repo::transaction()->create(
            $proposalModel->client_user_id,
            'fees',
            null, null,
            -$fees,
            "Taxa de transação via cartão de crédito" . $contractModel->id
        );

        // proposal update
        Repo::proposal()->updatePhaseConcluded($proposalModel->id);

        // gcm the gang
        $proName = $proposalModel->proUser->name;
        $gcmMessage = "Seu contrato foi aceito por $proName com sucesso. Acesse o menu [CONTRATOS] para mais informações!";
        $gcmPayload = ['reason' => 'proposal_update'];
        Gcm::to($proposalModel->client_user_id)
            ->send(new GcmBasicBody($gcmMessage,'Contrato', $gcmPayload));

        //
        return $this->responseOk();
    }

    public function destroy(Proposal $proposal)
    {
        $uid = \Auth::id();

        //
        if ($proposal->client_user_id != $uid) {
            return $this->responseError('Você não tem autorização para remover esta proposta. Apenas o cliente pode fazê-lo.');
        }

        //
        if ($proposal->phase == 3) {
            return $this->responseError('Esta proposta já está vinculada à um contrato. Não é possível removê-la.');
        }

        // backup proposal model
        $proposalClientName = $proposal->clientUser->name;
        $proposalProUserId = $proposal->pro_user_id;

        // remove model
        Repo::proposal()->destroy($proposal->id);

        // message pro about proposal removal
        $message = "A proposta feita por $proposalClientName foi removida.";
        Gcm::to($proposalProUserId)
                ->send(new GcmBasicBody(
                    $message,
                    'Proposta removida',
                    ['reason' => 'proposal_destroy']
                ));

        //
        return $this->responseOk();
    }

}
