<?php

namespace App\Http\Controllers;

use App\Adon\Commander\CommanderTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, CommanderTrait;


    public function responseIt($message, $status, $payload) {
        return response()
            ->json(compact('message', 'payload'))
            ->setStatusCode($status);
    }

    public function responseOk($payload=null, $message = 'Operação executada com êxito.', $status=200) {
        return $this->responseIt($message, $status, $payload);
    }

    public function responseError($message, $status=400) {
        return $this->responseIt($message, $status, null);
    }
}
