<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Adon\Gcm\Bodies\GcmBasicBody;
use App\Adon\Gcm\Gcm;
use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class ApiAdController extends Controller
{

    public function index()
    {
        $list = Repo::ad()->getAllNotExpired();

        return $this->responseOk($list);
    }

    public function find(Ad $ad)
    {
        return $this->responseOk($ad);
    }

    public function store()
    {
        $client_user_id = \Auth::id();

        $data = request()->validate([
            'body' => 'required',
            'expire_at' => 'required'
        ]);

        $ad = Repo::ad()->store($client_user_id, $data['body'], $data['expire_at']);

        return $this->responseOk(compact('ad'));
    }

    public function subscribe(Ad $ad)
    {
        // expired?
        if ($ad->isExpired()) {
            return $this->responseError('Este anúncio já expirou.');
        }

        // get fields
        $price = request()->validate([
            'price' => 'required'
        ])['price'];

        // save into db
        Repo::ad()->subscribe($ad->id, \Auth::id(), $price);

        // gcm 'em
        $adId = $ad->id;
        $gcmMessage = "Seu anúncio #$adId foi respondido!";
        $gcmPayload = ['reason' => 'ad_subscribe'];
        Gcm::to($ad->client_user_id)
            ->send(new GcmBasicBody($gcmMessage,'Anúncio', $gcmPayload));

        // return
        return $this->responseOk();
    }

    public function unsubscribe(Ad $ad)
    {
        // expired?
        if ($ad->isExpired()) {
            return $this->responseError('Este anúncio já expirou.');
        }

        //
        Repo::ad()->unsubscribe($ad->id, \Auth::id());

        //
        return $this->responseOk();
    }

    public function destroy(Ad $ad)
    {
        $userId = \Auth::id();

        if ($ad->client_user_id != $userId)
        {
            return $this->responseError('Você não é o criador deste anúncio, por isso não pode apagá-lo.');
        }

        Repo::ad()->destroy($ad->id);

        return $this->responseOk();
    }

}
