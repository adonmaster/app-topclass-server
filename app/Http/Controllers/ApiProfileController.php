<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use App\AttachmentTypeImage;
use Illuminate\Http\Request;

class ApiProfileController extends Controller
{
    public function index()
    {
        $user = Repo::user()->find(\Auth::id());

        return $this->responseOk(compact('user'));
    }

    public function store()
    {
        $user = \Auth::user();

        $name = request('name');
        if ($name) {
            $user = Repo::user()->saveName($user, $name);
        }

        $file = request()->file('avatar');
        if ($file)  {
            $attachmentModel = Repo::attachment()->saveSingle($user, $file, new AttachmentTypeImage());
            $user = Repo::user()->saveAvatar($user, $attachmentModel);
        }

        return $this->responseOk(compact('user'));
    }
}
