<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class ApiProRequisitionController extends Controller
{

    public function index()
    {
        return $this->responseOk(Repo::proRequisition()->getAllPendingFor(\Auth::id()));
    }

    public function store()
    {
        $user = \Auth::user();
        $cv = request()->file('cv');
        $obs = request('obs');

        Repo::proRequisition()->save($user, $cv, $obs);

        return $this->responseOk();
    }

}
