<?php

namespace App\Http\Controllers;

use App\Adon\Flash\Flash;
use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index()
    {
        return view('admin.index');
    }

    public function proRequisition()
    {
        $requisitions = Repo::proRequisition()->getAllPendingFor(null);

        return view('admin.index-pro-requisition', compact('requisitions'));
    }

    public function proRequisitionDestroy()
    {
        if (Repo::proRequisition()->destroy(request('id')))
        {
            Flash::success('Requisição apagada com sucesso!');
        }
        else
        {
            Flash::error('Requisição não encontrada!');
        }

        return redirect()->back();
    }

    public function proRequisitionConfirm()
    {
        if ($model = Repo::proRequisition()->resolve(request('id')))
        {
            Repo::user()->savePro($model->user_id, true);

            Flash::success('Requisição Aceita com sucesso!');
        }
        else
        {
            Flash::error('Requisição não encontrada!');
        }


        return redirect()->back();
    }

}
