<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class ApiSkillController extends Controller
{

    public function index()
    {
        $skills = Repo::skill()->getForAuth();

        return $this->responseOk(compact('skills'));
    }

    public function store()
    {
        $skills = request('skills') ?: [];

        Repo::skill()->savePackFor(\Auth::id(), $skills);

        return $this->responseOk();
    }

}
