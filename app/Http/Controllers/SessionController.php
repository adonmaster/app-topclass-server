<?php

namespace App\Http\Controllers;

use App\Adon\Flash\Flash;
use Illuminate\Http\Request;

class SessionController extends Controller
{

    public function login()
    {
        return view('login');
    }

    public function storeLogin()
    {
        $data = request()->validate([
            'email' => 'required',
            'password' => 'required|min:5'
        ]);

        if (\Auth::attempt($data) && \Auth::user()->is_admin)
        {
            return redirect()->route('admin.index');
        }

        Flash::error('Usuário/senha incorreto(s) para um administrador');

        \Auth::logout();

        return redirect()->route('login');
    }

    public function logoff()
    {
        \Auth::logout();

        return url('/');
    }

}
