<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class ApiTransactionController extends Controller
{

    public function index()
    {
        $list = Repo::transaction()->paginateFor(\Auth::id());

        return $this->responseOk($list);
    }

}
