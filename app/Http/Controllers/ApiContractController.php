<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class ApiContractController extends Controller
{

    public function index()
    {
        $list = Repo::contract()->listRelated(\Auth::id());

        return $this->responseOk($list);
    }

}
