<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class ApiGcmController extends Controller
{
    public function store()
    {
        $data = request()->validate([
            'gcm' => 'required',
            'token' => 'required'
        ]);

        /**
         * @var $gcm
         * @var $token
         */
        extract($data);

        Repo::user()->saveGcm(\Auth::id(), $gcm, $token);

        return $this->responseOk();
    }
}
