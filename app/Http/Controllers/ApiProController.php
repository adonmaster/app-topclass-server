<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use Illuminate\Http\Request;

class ApiProController extends Controller
{

    public function query()
    {
        $q = request('q');

        $pros = Repo::user()->proQuery($q);

        return $this->responseOk($pros);
    }

}
