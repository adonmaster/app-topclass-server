<?php

namespace App\Http\Controllers;

use App\Adon\Repo\Repo;
use App\Mail\Register\CodeMail;
use Illuminate\Http\Request;

class ApiRegisterController extends Controller
{

    public function register()
    {
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email'
        ]);

        $user = Repo::user()->register($data['email'], $data['name']);

        $registration = Repo::userRegistration()->findBy($user->id);

        \Mail::send(new CodeMail($user->email, $registration->code));

        return $this->responseOk();
    }

    public function code()
    {
        $data = request()->validate([
            'email' => 'required|email',
            'code' => 'required'
        ]);

        $user = Repo::user()->findBy($data['email']);
        if ( ! $user) return $this->responseError('Email não encontrado', 404);

        $checkedResult = Repo::userRegistration()->check($user->id, $data['code']);
        if ($errorMsg = $checkedResult->getError()) return $this->responseError($errorMsg);

        // reassign
        $user = Repo::user()->confirmRegistration($user->id);

        return $this->responseOk([
            'id' => $user->id,
            'api_token' => $user->api_token,
            'avatar' => $user->avatar,
            'avatar_thumb' => $user->avatar_thumb,
            'is_pro' => $user->is_pro
        ]);
    }

}
