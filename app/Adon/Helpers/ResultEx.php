<?php namespace App\Adon\Helpers;


use Closure;

class ResultEx
{
    // static
    public static function success($payload) {
        return new ResultEx(true, $payload, null);
    }

    public static function failure($reason) {
        return new ResultEx(false, null, $reason);
    }

    // instance

    private $payload;
    private $errorReason = '';
    private $wasSuccessful = false;

    /**
     * ResultEx constructor.
     * @param $payload
     * @param string $errorReason
     * @param bool $wasSuccessful
     */
    public function __construct($payload, $wasSuccessful, $errorReason)
    {
        $this->payload = $payload;
        $this->errorReason = $errorReason;
        $this->wasSuccessful = $wasSuccessful;
    }

    public function getError() {
        if ($this->wasSuccessful) return false;
        return $this->errorReason;
    }

    public function onSuccess(Closure $cb) {
        if ($this->wasSuccessful) $cb($this->payload);
    }

    public function onError(Closure $cb) {
        if (!$this->wasSuccessful) $cb($this->errorReason);
    }
}