<?php namespace App\Adon\Repo;


use App\Attachable;
use App\Attachment;
use App\AttachmentType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class AttachRepo
{

    /**
     * @param Attachable|Model $model
     * @param UploadedFile $file
     * @param AttachmentType $type
     * @return Attachment
     */
    public function saveSingle(Model $model, UploadedFile $file, AttachmentType $type)
    {
        // clean up
        $this->destroyFor($model);

        // save file
        $resType = $type->saveFile($file);
        return $model->attachments()->create([
            'type' => $type->desc(),
            'server' => $resType->server,
            'path' => $resType->path,
            'path_thumb' => $resType->path_thumb,
            'url' => $resType->url,
            'url_thumb' => $resType->url_thumb
        ]);
    }

    /**
     * @param Attachable|Model $model
     */
    private function removeFiles(Model $model)
    {
        /**
         * @var Attachment $model
         */
        foreach ($model->attachments()->get() as $model) {
            \File::delete([
                $model->path,
                $model->path_thumb,
            ]);
        }
    }

    /**
     * @param Attachable|Model $model
     */
    public function destroyFor($model)
    {
        $this->removeFiles($model);
        $model->attachments()->delete();
    }

}