<?php namespace App\Adon\Repo;


use App\ProSkill;
use App\Skill;

class SkillRepo
{

    public function getForAuth()
    {
        return Skill::with('proSkillAuth')->get();
    }

    public function savePackFor($userId, array $skills)
    {
        foreach ($skills as $item) {
            $skillId = $item['id'];
            $option = $item['option'] ?: false;
            $comp = isset($item['comp']) ? $item['comp'] : null;

            $this->saveSingle($userId, $skillId, $option, $comp);
        }
    }

    /**
     * @param $userId
     * @param $skillId
     * @param $option
     * @param $comp
     * @return $this|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|static[]|$ProSkill
     */
    private function saveSingle($userId, $skillId, $option, $comp)
    {
        $proSkill = ProSkill
            ::where('user_id', $userId)
            ->where('skill_id', $skillId)
            ->first();

        if ( ! $proSkill)
        {
            return ProSkill::create([
                'skill_id' => $skillId,
                'user_id' => $userId,
                'option' => $option,
                'comp' => $comp
            ]);
        }
        else
        {
            $proSkill->option = $option;
            $proSkill->comp = $comp;
            $proSkill->save();

            return $proSkill;
        }
    }

}