<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 06-Mar-20
 * Time: 18:57
 */

namespace App\Adon\Repo;


use App\Transaction;


class TransactionRepo
{

    /**
     * @param $user_id
     * @param $operation_type
     * @param $sourceable_type
     * @param $sourceable_id
     * @param $amount
     * @param $desc
     * @return Transaction|$this|\Illuminate\Database\Eloquent\Model
     */
    public function create($user_id, $operation_type, $sourceable_type, $sourceable_id, $amount, $desc)
    {
        $lastUserTransaction = Transaction
            ::where('user_id', $user_id)
            ->limit(1)
            ->orderByDesc('id')
            ->first();

        $lastBalance = $lastUserTransaction ? $lastUserTransaction->balance : 0;
        $balance = $lastBalance + $amount;

        return Transaction::create(compact(
            'user_id', 'operation_type', 'sourceable_type', 'sourceable_id',
            'amount', 'desc', 'balance'
        ));
    }

    public function paginateFor($user_id)
    {
        return Transaction
            ::where(compact('user_id'))
            ->orderByDesc("id")
            ->simplePaginate(150);
    }
}