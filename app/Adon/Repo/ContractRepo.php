<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 06-Mar-20
 * Time: 19:20
 */

namespace App\Adon\Repo;


use App\Contract;
use App\Proposal;

class ContractRepo
{

    /**
     * @param Proposal $proposalModel
     * @return Contract|$this|\Illuminate\Database\Eloquent\Model
     */
    public function createFrom(Proposal $proposalModel)
    {
        $pro_user_id = $proposalModel->pro_user_id;
        $client_user_id = $proposalModel->client_user_id;
        $body = $proposalModel->body;
        $due_at = $proposalModel->due_at;
        $was_executed = false;
        $price = $proposalModel->price;

        return Contract::create(compact(
            'pro_user_id', 'client_user_id', 'body', 'due_at', 'was_executed', 'price'
        ));
    }

    public function listRelated($user_id)
    {
        return Contract
            ::where('pro_user_id', $user_id)
            ->orWhere('client_user_id', $user_id)
            ->orderByDesc('id')
            ->get();
    }

}