<?php namespace App\Adon\Repo;


use App\Adon\Helpers\ResultEx;
use App\UserRegistration;
use Carbon\Carbon;

class UserRegistrationRepo
{

    /**
     * @param int $user_id
     * @return UserRegistration
     */
    public function registerFor($user_id)
    {
        $code = random_int(100000, 999999);
        $expired_at = Carbon::now()->addHours(4);

        return UserRegistration::updateOrCreate(compact('user_id'), compact('code', 'expired_at'));
    }

    /**
     * @param $user_id
     * @return UserRegistration|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function findBy($user_id) {
        return UserRegistration::where(compact('user_id'))->first();
    }

    /**
     * @param int $user_id
     * @param $code
     * @return ResultEx
     */
    public function check($user_id, $code)
    {
        $model = UserRegistration::where(compact('user_id', 'code'))->first();
        if ( ! $model) return ResultEx::failure('Código incorreto.');

        if (Carbon::now()->isAfter($model->expired_at)) return ResultEx::failure('Código expirou, refaça o registro.');

        return ResultEx::success($model);
    }

}