<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 25-Jan-20
 * Time: 12:18
 */

namespace App\Adon\Repo;


use App\Proposal;
use Carbon\Carbon;

class ProposalRepo
{

    /**
     * @param $client_user_id
     * @param $pro_user_id
     * @param $body
     * @param $price
     * @param $phase
     * @param $order_customer_card_token
     * @param $order_customer_name
     * @param $expire_at
     * @param $due_at
     * @return Proposal
     */
    public function create($client_user_id, $pro_user_id, $body, $price, $phase, $order_customer_card_token, $order_customer_name, $expire_at, $due_at)
    {
        return Proposal::create(compact(
            'client_user_id', 'pro_user_id', 'body', 'price',
            'phase', 'order_customer_card_token', 'order_customer_name',
            'expire_at', 'due_at'
        ));
    }

    public function getRelated($userId)
    {
        return Proposal::where(function($q) use ($userId) {
            $q->where('client_user_id', $userId)
                ->orWhere('pro_user_id', $userId);
            })
            ->orderBy('id')
            ->get();
    }

    /**
     * @param $id
     * @return Proposal|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function find($id)
    {
        return Proposal::find($id);
    }

    /**
     * @param $id
     */
    public function updatePhaseConcluded($id)
    {
        $phase = Proposal::PHASE_CONCLUDED;

        Proposal::where('id', $id)->update(compact('phase'));
    }

    public function destroy($id)
    {
        Proposal::where(compact('id'))->delete();
    }

}