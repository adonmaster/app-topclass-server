<?php namespace App\Adon\Repo;


abstract class BaseRepo
{
    /**
     * @param $eloquentClass
     * @param $q
     * @param array $fieldsToSearch
     * @param string $orderedBy
     * @param int $limit
     * @return Builder
     */
    protected function qBuilder($eloquentClass, $q, array $fieldsToSearch, $orderedBy='updated_at', $limit=25)
    {
        $list = array_filter(array_map('trim', explode(' ', $q ?: '')), 'strlen');

        return $eloquentClass::where(function($query) use($list, $fieldsToSearch) {
            foreach ($list as $item) {
                $query->where(function($qq) use($list, $item, $fieldsToSearch) {
                    foreach ($fieldsToSearch as $index=>$field) {
                        if ($index == 0) {
                            $qq->where($field, 'like', "%$item%");
                        } else {
                            $qq->orWhere($field, 'like', "%$item%");
                        }
                    }
                });
            }
        })
            ->orderBy($orderedBy)
            ->limit($limit);
    }

}