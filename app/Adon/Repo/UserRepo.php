<?php namespace App\Adon\Repo;


use App\Attachment;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;

class UserRepo extends BaseRepo
{

    /**
     * @param string $email
     * @param string $name
     * @return User
     */
    public function register($email, $name)
    {
        $model = $this->findOrCreate($email, $name);

        Repo::userRegistration()->registerFor($model->id);

        return $model;
    }

    /**
     * @param $email
     * @param $name
     * @return $this|UserRepo|User|\Illuminate\Database\Eloquent\Model|null|object
     */
    private function findOrCreate($email, $name)
    {
        $model = $this->findBy($email);
        if (!$model) {
            $api_token = '';
            $email_verified_at = null;
            $password = Str::random(8);
            $is_pro = false;
            $model = User::create(compact('name', 'email', 'api_token', 'password', 'is_pro'));
        }
        return $model;
    }

    /**
     * @param int $id
     * @return User
     */
    public function confirmRegistration($id)
    {
        $model = User::findOrFail($id);
        $model->api_token = Str::random(60);
        $model->password = Str::random(8);
        $model->email_verified_at = Carbon::now();
        $model->update();

        return $model->refresh();
    }

    /**
     * @param $email
     * @return \Illuminate\Database\Eloquent\Model|null|object|static|User
     */
    public function findBy($email)
    {
        return User::where(compact('email'))->first();
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function find($id)
    {
        return User::find($id);
    }

    public function listAuthEventCheckoutCount($emails, $type)
    {
        return User
            ::with(['fromAuthEventCheckCount' => function($q) use ($type) {
                $q->where('type', $type);
            }])
            ->whereIn('email', $emails)
            ->get();

    }

    /**
     * @param User $user
     * @param $name
     * @return User
     */
    public function saveName(User $user, $name)
    {
        $user->name = $name;
        $user->save();

        return $user;
    }

    /**
     * @param User $user
     * @param Attachment $model
     * @return User
     */
    public function saveAvatar(User $user, Attachment $model)
    {
        $user->avatar = $model->url;
        $user->avatar_thumb = $model->url_thumb;
        $user->save();

        return $user;
    }

    /**
     * @param int $user_id
     * @param boolean $option
     */
    public function savePro($user_id, $option)
    {
        User::where('id', $user_id)->update(['is_pro' => $option]);
    }

    public function proQuery($q)
    {
//        $baseQuery = $this->qBuilder(
//            User::class, $q,
//            ['name', 'email', 'skill.desc'],
//            'updated_at', 25
//        );
//
//        return $baseQuery
//            ->with('skills')
//            ->get();

        /**
         * select distinct u.* from users u
        left join pro_skill ps on u.id = ps.user_id
        left join skills s on s.id = ps.skill_id
        where (
        u.name like '%port%' or u.email like '%port%'
        or s.`desc` like '%port%' or ps.comp like '%port%'
        )
         */

        $list = array_filter(array_map('trim', explode(' ', $q ?: '')), 'strlen');
        $fieldsToSearch = ['u.name', 'u.email', 's.desc', 'ps.comp'];

        $proIds = \DB::table('users as u')
            ->selectRaw('u.id')
            ->distinct()
            ->leftJoin('pro_skill as ps', 'u.id', 'ps.user_id')
            ->leftJoin('skills as s', 's.id', 'ps.skill_id')
            ->where('is_pro', true)
            ->limit(100)
            ->where(function($query) use ($list, $fieldsToSearch) {
                if (count($list)==0) return;
                foreach ($list as $item) {
                    $query->where(function($qq) use($list, $item, $fieldsToSearch) {
                        foreach ($fieldsToSearch as $index=>$field) {
                            if ($index == 0) {
                                $qq->where($field, 'like', "%$item%");
                            } else {
                                $qq->orWhere($field, 'like', "%$item%");
                            }
                        }
                    });
                }
            })->pluck('id');

        return User
            ::with('skills')
            ->whereIn('id', $proIds)
            ->get();

    }

    /**
     * @param int $userId
     * @param string $gcm
     * @param $gcm_token
     */
    public function saveGcm($userId, $gcm, $gcm_token)
    {
        User::where(['id' => $userId])
            ->update(compact('gcm', 'gcm_token'));
    }


}