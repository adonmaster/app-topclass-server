<?php namespace App\Adon\Repo;


use App\Adon\Flash\Flash;
use App\AttachmentTypeAny;
use App\ProRequisition;
use App\User;
use Illuminate\Http\UploadedFile;

class ProRequisitionRepo
{

    /**
     * @param User $user
     * @param UploadedFile|null $cv
     * @param $obs
     * @return ProRequisition
     */
    public function save(User $user, $cv, $obs)
    {
        $model = ProRequisition::create([
            'user_id' => $user->id,
            'obs' => $obs
        ]);

        // cv?
        if ($cv) Repo::attachment()->saveSingle($model, $cv, new AttachmentTypeAny());

        return $model;
    }

    public function getAllPendingFor($user_id)
    {
        // user_id == null?
        if (is_null($user_id)) return ProRequisition::pending()->orderByDesc('id')->get();

        return ProRequisition::pending()
            ->where(compact('user_id'))
            ->orderByDesc('id')
            ->get();
    }

    /**
     * @param $id
     * @return bool
     */
    public function destroy($id)
    {
        $model = ProRequisition::find($id);

        if ( ! $model) return false;

        Repo::attachment()->destroyFor($model);

        $model->delete();

        return true;
    }

    /**
     * @param $id
     * @return bool|ProRequisition
     */
    public function resolve($id)
    {
        $model = ProRequisition::find($id);

        if ( ! $model) return false;

        $model->is_resolved = true;
        $model->save();

        return $model;
    }

}