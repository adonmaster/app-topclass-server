<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 08-Apr-20
 * Time: 20:09
 */

namespace App\Adon\Repo;


use App\Ad;
use App\AdPro;

class AdRepo
{

    /**
     * @param $client_user_id
     * @param $body
     * @param $expire_at
     * @return $this|\Illuminate\Database\Eloquent\Model|Ad
     */
    public function store($client_user_id, $body, $expire_at)
    {
        return Ad::create(compact('client_user_id', 'body', 'expire_at'));
    }

    public function destroy($adId)
    {
        Ad::where('id', $adId)->delete();
    }

    /**
     * @param $ad_id
     * @param $pro_user_id
     * @param $price
     * @return \Illuminate\Database\Eloquent\Model|static|AdPro
     */
    public function subscribe($ad_id, $pro_user_id, $price)
    {
        $model = AdPro::updateOrCreate(
            compact('ad_id', 'pro_user_id'),
            compact('price')
        );

        return $model;
    }

    public function unsubscribe($ad_id, $pro_user_id)
    {
        AdPro::where(compact('ad_id', 'pro_user_id'))->delete();
    }

    public function getAllNotExpired()
    {
        return Ad::notExpired()->orderByDesc('id')->get();
    }
}