<?php namespace App\Adon\Repo;


class Repo
{

    private static $cache = [];
    private static function retrieve($class) {
        if ( ! isset(self::$cache[$class])) self::$cache[$class] = \App::make($class);
        return self::$cache[$class];
    }

    /**
     * @return UserRepo
     */
    public static function user() {
        return self::retrieve(UserRepo::class);
    }

    /**
     * @return UserRegistrationRepo
     */
    public static function userRegistration() {
        return self::retrieve(UserRegistrationRepo::class);
    }

    /**
     * @return AttachRepo
     */
    public static function attachment() {
        return self::retrieve(AttachRepo::class);
    }

    /**
     * @return ProRequisitionRepo
     */
    public static function proRequisition() {
        return self::retrieve(ProRequisitionRepo::class);
    }

    /**
     * @return SkillRepo
     */
    public static function skill() {
        return self::retrieve(SkillRepo::class);
    }

    /**
     * @return ProposalRepo
     */
    public static function proposal() {
        return self::retrieve(ProposalRepo::class);
    }

    /**
     * @return TransactionRepo
     */
    public static function transaction() {
        return self::retrieve(TransactionRepo::class);
    }

    /**
     * @return ContractRepo
     */
    public static function contract() {
        return self::retrieve(ContractRepo::class);
    }

    /**
     * @return AdRepo
     */
    public static function ad() {
        return self::retrieve(AdRepo::class);
    }

}