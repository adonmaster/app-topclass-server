<?php namespace App\Adon\Gcm\Adapters;


use App\Adon\Gcm\GcmBody;
use App\User;
use GuzzleHttp\Client as GuzzleClient;
use paragraph1\phpFCM\Client as FcmClient;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Notification;
use paragraph1\phpFCM\Recipient\Device;

class FcmAdapter
{

    /**
     * @var User[]
     */
    private $users;
    /**
     * @var GcmBody
     */
    private $body;

    /**
     * @param User[] $users
     * @param GcmBody $body
     * ApnAdapter constructor.
     */
    public function __construct($users, GcmBody $body)
    {
        $this->users = $users;
        $this->body = $body;
    }


    /**
     * @param User[] $users
     * @param GcmBody $body
     * @return FcmAdapter
     */
    public static function f($users, GcmBody $body)
    {
        return new FcmAdapter($users, $body);
    }

    public function send()
    {
        $filteredUsers = $this->filtered();
        if (count($filteredUsers)==0) return;

        $serverKey = getenv('FCM_SERVER_KEY');

        $client = new FcmClient();
        $client->setApiKey($serverKey);

        $client->injectHttpClient(new GuzzleClient());

        $message = new Message();
        foreach ($filteredUsers as $user) {
            $message->addRecipient(new Device($user->getGcmToken()));
        }

        /*
         * *** ATENÇÃO ***
         *
         * setNotification: ativa FirebaseMessagingService apenas no foreground, e notifica sozinho no bknd
         * Se omitir o notification e usar setData:
         * -- não cria notificação, mas sempre envia para o FirebaseMessagingService
         * Ou seja, se não usar notification, eh trabalho do app em criá-la
         *
         * Daki pra frente soh vou enviar 'data', e ignorar notification...
         * ... eh seu trabalho criar a notificação no app!
         *
         * PS.: qdo o app estah no modo debug, se enviar notification, não ativa no bknd
        */

        $message->setData([
            'title' => $this->body->getTitle(),
            'body' => $this->body->getMessage(),
            'payload' => $this->body->getPayload(),
            'badge' => $this->body->getBadge()
        ]);

        $response = $client->send($message);

        $statusCode = $response->getStatusCode();
        if ($statusCode >= 400) {
            throw new GcmException($response->getBody(), $statusCode);
        }
    }

    /**
     * @return User[]
     */
    private function filtered()
    {
        return array_filter($this->users, function ($user) {
            /**
             * @var User $user
             */
            return $user->isGcmFcmService();
        });
    }

}