<?php namespace App\Adon\Gcm\Adapters;


use App\Adon\Apn\ApnManager;
use App\Adon\Gcm\GcmBody;
use App\User;

class ApnAdapter
{
    /**
     * @var User[]
     */
    private $users;
    /**
     * @var GcmBody
     */
    private $body;

    /**
     * @param User[] $users
     * @param GcmBody $body
     * ApnAdapter constructor.
     */
    public function __construct($users, GcmBody $body)
    {
        $this->users = $users;
        $this->body = $body;
    }


    /**
     * @param User[] $users
     * @param GcmBody $body
     * @return ApnAdapter
     */
    public static function f($users, GcmBody $body)
    {
        return new ApnAdapter($users, $body);
    }

    public function send()
    {
        $filteredUsers = $this->filtered();
        if (count($filteredUsers)==0) return;

        $tokens = array_map(function(User $user) {
            return $user->getGcmToken();
        }, $filteredUsers);

        try {
            ApnManager::newSender()->send(
                $tokens,
                $this->body->getTitle(),
                $this->body->getMessage(),
                $this->body->getPayload()
            );
        }
        catch (\Exception $e) {
            throw new GcmException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return User[]
     */
    private function filtered()
    {
        return array_filter($this->users, function ($user) {
            /**
             * @var User $user
             */
            return $user->isGcmApnService();
        });
    }
}