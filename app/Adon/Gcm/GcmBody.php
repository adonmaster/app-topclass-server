<?php namespace App\Adon\Gcm;


interface GcmBody
{

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @return array
     */
    public function getPayload();

    /**
     * @return int
     */
    public function getBadge();

}