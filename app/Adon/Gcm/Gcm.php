<?php namespace App\Adon\Gcm;


use App\Adon\Repo\Repo;
use App\User;
use Illuminate\Support\Collection;

class Gcm
{
    /**
     * @var int[]
     */
    private $users;

    /**
     * Gcm constructor.
     */
    private function __construct() {}

    /**
     * @param User|int|User[]|int[]|Collection $userIdList
     * @return Gcm
     */
    public static function to($userIdList)
    {
        if ($userIdList instanceof Collection) return self::to($userIdList->all());
        if ( ! is_array($userIdList)) return self::to([$userIdList]);

        return tap(new self(), function($v) use ($userIdList) {
            /**
             * @var Gcm $v
             */
            $v->setUsers($userIdList);
        });
    }

    private function setUsers($userIdList)
    {
        $this->users = $userIdList;
    }

    public function send(GcmBody $body)
    {
        $userList = $this->grantUserModels();

        GcmSender::f($userList, $body)->send();
    }

    /**
     * @return User[]
     */
    private function grantUserModels()
    {
        $users = array_map(function($item) {
            return ($item instanceof User)
                ? $item
                : Repo::user()->find($item);
        }, $this->users);

        return array_filter($users, function($i) { return !is_null($i); });
    }
}