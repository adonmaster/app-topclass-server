<?php namespace App\Adon\Gcm;


use App\Adon\Gcm\Adapters\ApnAdapter;
use App\Adon\Gcm\Adapters\FcmAdapter;
use App\User;

class GcmSender
{
    /**
     * @var User[]
     */
    private $users;

    /**
     * @var GcmBody
     */
    private $body;

    /**
     * GcmSender constructor.
     * @param User[] $users
     * @param GcmBody $body
     */
    private function __construct($users, GcmBody $body)
    {
        $this->users = $users;
        $this->body = $body;
    }


    /**
     * @param User[] $users
     * @param GcmBody $body
     * @return GcmSender
     */
    public static function f($users, GcmBody $body)
    {
        return new GcmSender($users, $body);
    }

    public function send()
    {
        if (getenv('GCM_BLOCK')=='true')
        {
            foreach ($this->users as $user)
            {
                $email = $user->email;
                $title = $this->body->getTitle();
                $msg = $this->body->getMessage();
                \Log::info("messaging $email: [$title => $msg]");
            }
        }
        else
        {
            ApnAdapter::f($this->users, $this->body)->send();
            FcmAdapter::f($this->users, $this->body)->send();
        }
    }

}