<?php namespace App\Adon\Gcm\Bodies;


use App\Adon\Gcm\GcmBody;

class GcmBasicBody implements GcmBody
{
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $payload;

    /**
     * GcmBasicBody constructor.
     * @param string $message
     * @param string $title
     * @param array $payload
     */
    public function __construct($message, $title, $payload)
    {
        $this->message = $message;
        $this->title = $title;
        $this->payload = $payload;
    }


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return int
     */
    public function getBadge()
    {
        return 1;
    }
}