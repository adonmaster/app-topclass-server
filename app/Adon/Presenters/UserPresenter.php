<?php namespace App\Adon\Presenters;


use App\User;

class UserPresenter
{

    /**
     * @var User
     */
    private $model;

    /**
     * UserPresenter constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }


    public function avatarThumb()
    {
        return $this->model->avatar_thumb ?: asset('imgs/empty.png');
    }

}