<?php namespace App\Adon\Apn;


class ApnException extends \Exception {

    private static $CODES = [
        0 => "No errors encountered",
        1 => "Processing error",
        2 => "Missing device token",
        3 => "Missing topic",
        4 => "Missing payload",
        5 => "Invalid token size",
        6 => "Invalid topic size",
        7 => "Invalid payload size",
        8 => "Invalid token",
        10 => "Shutdown",
        128 => "Protocol error (APNs could not parse the notification)",
        255 => "None (unknown)"
    ];


    /**
     * @param $binaryResponse
     *
     * @throws ApnException
     */
    public static function throwFromBinary($binaryResponse)
    {
        // check
        if ($binaryResponse !== false && strlen($binaryResponse) == 6)
        {
            $pack = unpack('Ccommand/Cstatus_code/Nidentifier', $binaryResponse);

            $command = $pack['command'];
            $code = $pack['status_code'];
            $identifier = $pack['identifier'];

            // let's do it!
            if ($code != 0) throw new self(self::$CODES[$code]);
        }
    }


} 