<?php namespace App\Adon\Apn;


class ApnManager {

    /**
     * @return ApnSender
     */
    public static function newSender()
    {
        $isSandbox = getenv('APN_IS_SANDBOX') == 'true';

        $certificateFile = __DIR__ . ($isSandbox ? '/aps_dev.pem' : '/aps_prod.pem');

        $passPhrase = 'adon';

        return new ApnSender($certificateFile, $passPhrase, $isSandbox);
    }

}