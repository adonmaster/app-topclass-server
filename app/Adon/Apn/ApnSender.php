<?php namespace App\Adon\Apn;


class ApnSender {

    private static $HOST_SANDBOX = 'gateway.sandbox.push.apple.com';
    private static $PORT_SANDBOX = 2195;

    private static $HOST = 'gateway.push.apple.com';
    private static $PORT = 2195;

    // shared

    private static $conn = null;

    /**
     * @var string
     */
    private $pemPath;

    /**
     * @var string
     */
    private $certificatePassPhrase;

    /**
     * @var bool
     */
    private $isSandbox;

    /**
     * @param string $certificatePemPath
     * @param string $certificatePassPhrase
     * @param bool $isSandbox
     */
    function __construct($certificatePemPath, $certificatePassPhrase, $isSandbox)
    {
        $this->pemPath = $certificatePemPath;
        $this->certificatePassPhrase = $certificatePassPhrase;
        $this->isSandbox = $isSandbox;
    }

    /**
     * @return string
     */
    private function host()
    {
        return $this->isSandbox ? self::$HOST_SANDBOX : self::$HOST;
    }

    /**
     * @return int
     */
    private function port() {
        return $this->isSandbox ? self::$PORT_SANDBOX : self::$PORT;
    }

    /**
     * @param string|string[] $token
     * @param string $title
     * @param string $body
     * @param null $extra
     * @param int $badge
     * @param int $sound
     * @throws ApnException
     */
    public function send($token, $title, $body, $extra = null, $badge = 1, $sound = 0)
    {
        $exceptions = [];
        $tokenList = is_array($token) ? $token : [$token];

        $this->connect2();

        foreach ($tokenList as $tokenSingle)
        {
            $payload = $this->generatePayload($tokenSingle, $title, $body, $extra, $badge, $sound);

            if ( ! fwrite(self::$conn, $payload))
            {
                // well, it seems there's some error.

                // wait a little to not crop the 'write' operation, and
                // start to read at the 'almost' right time.
                usleep(100000);

                try
                {
                    // read from server
                    ApnException::throwFromBinary(fread(self::$conn, 6));
                }
                catch (\Exception $e)
                {
                    $exceptions[$tokenSingle] = $e->getMessage();
                }
            }
        }

        if (count($exceptions))
        {
            $this->disconnect();

            throw new ApnException(json_encode($exceptions));
        }
    }

    /**
     * @throws ApnException
     */
    private function connect2() {
        $context = stream_context_create();
        stream_context_set_option($context, 'ssl', 'local_cert', $this->pemPath);
        stream_context_set_option($context, 'ssl', 'passphrase', $this->certificatePassPhrase);
        $host = 'ssl://' . $this->host() . ':' . $this->port();
        self::$conn = stream_socket_client($host, $error, $errorString, ini_get('default_socket_timeout'), STREAM_CLIENT_CONNECT, $context);
        if (!self::$conn) {
            throw new ApnException($errorString ?: 'Não consigo no conectar no servidor APN da apple!');
        }
    }

    /**
     * @deprecated Use ApnSender::connect2()
     * @throws ApnException
     */
    private function connect()
    {
        if (self::$conn) { return; }

        // otherwise

        $context = stream_context_create([
            'ssl' => [
                'local_cert' => $this->pemPath,
                'passphrase' => $this->certificatePassPhrase,
            ],
        ]);

        $host = 'ssl://' . $this->host() . ':' . $this->port();

        self::$conn = stream_socket_client(
            $host,
            $error,
            $errorString,
            ini_get('default_socket_timeout'),
            STREAM_CLIENT_CONNECT,
            $context
        );

        // well... it prevents the 'fRead' command
        // to block the thread for fucking
        // 10 minutes :)
        stream_set_blocking(self::$conn, 0);

        if ( ! self::$conn) throw new ApnException('Cant connect, buddy!');
    }

    /**
     * @param string $token
     * @param string $title
     * @param string $body
     * @param mixed $extra
     * @param int $badge
     * @param string $sound
     *
     * @return mixed
     */
    private function generatePayload($token, $title, $body, $extra, $badge, $sound)
    {
        $tokenSane = $this->sanitizeToken($token);

        $aps = [];
        if ($title)
        {
            $aps = [
                'alert' => compact('title', 'body'),
                'badge' => $badge,
                'sound' => $sound
            ];
        }
        else
        {
            $aps['content-available'] = 1;
        }
        if ($extra) $aps['extra'] = $extra;

        $payload = json_encode(compact('aps'));

        return chr(0) . chr(0) . chr(32) . pack('H*', $tokenSane) . chr(0) . chr(strlen($payload)) . $payload;
    }

    /**
     *
     */
    private function disconnect()
    {
        fclose(self::$conn);

        self::$conn = null;
    }

    /**
     * @param string $token
     * @return string
     */
    private function sanitizeToken($token)
    {
        return preg_replace('/[^\\da-fA-F]/', '', $token);
    }
}