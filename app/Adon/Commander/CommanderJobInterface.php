<?php namespace App\Adon\Commander;


interface CommanderJobInterface {

    function handle();

}