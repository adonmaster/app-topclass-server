<?php namespace App\Adon\Commander;


trait CommanderTrait {

    public function execute(CommanderJobInterface $job)
    {
        return $job->handle();
    }

}