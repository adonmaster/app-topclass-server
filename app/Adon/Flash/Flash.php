<?php namespace App\Adon\Flash;

class Flash
{
    public static function error($body, $title='Eita!') {
        self::alert('alert-danger', $title, $body);
    }

    public static function info($body, $title='Oba!') {
        self::alert('alert-info', $title, $body);
    }

    public static function success($body, $title='Maravilha!') {
        self::alert('alert-success', $title, $body);
    }

    public static function warning($body, $title = 'Nossa!')
    {
        self::alert('alert-warning', $title, $body);
    }

    public static function render()
    {
        if (\Session::has('adon-flash-type'))
        {
            $type = \Session::get('adon-flash-type');
            $title = \Session::get('adon-flash-title');
            $body = \Session::get('adon-flash-body');

            return "
                <div class=\"alert p-3 $type alert-dismissible\" role=\"alert\">
                <!--
                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                  -->
                  <strong>$title</strong> $body
                </div>
                ";
        }

        return '';
    }

    private static function alert($type, $title, $body)
    {
        \Session::flash('adon-flash-type', $type);
        \Session::flash('adon-flash-title', $title);
        \Session::flash('adon-flash-body', $body);
    }
}