<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProRequisition
 *
 * @package App
 * @property int $id
 * @property int $user_id
 * @property bool $is_resolved
 * @property string $obs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Attachment[] $attachments
 * @property-read int|null $attachments_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProRequisition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProRequisition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProRequisition pending()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProRequisition query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProRequisition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProRequisition whereIsResolved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProRequisition whereObs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProRequisition whereUserId($value)
 * @mixin \Eloquent
 */
class ProRequisition extends Model
{
    protected $table = 'pro_requisition';
    protected $fillable = ['user_id', 'obs', 'is_resolved'];
    protected $hidden = [];
    protected $dates = [];
    public $timestamps = false;
    protected $casts = [];
    protected $with = ['user', 'attachments'];


    # Scope
    public function scopePending($q)
    {
        $q->where('is_resolved', false);
    }

    # Relationships

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function attachments() {
        return $this->morphMany(Attachment::class, 'attachable');
    }

}
