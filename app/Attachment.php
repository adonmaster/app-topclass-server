<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserRegistration
 *
 * @property int $id
 * @property string $attachable_type
 * @property int $attachable_id
 * @property string $type
 * @property string $server
 * @property string $path
 * @property string $path_thumb
 * @property string $url
 * @property string $url_thumb
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $attachable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereAttachableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereAttachableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment wherePathThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereUrlThumb($value)
 * @mixin \Eloquent
 */
class Attachment extends Model
{
    protected $table = 'attachments';
    protected $fillable = [
        'attachable_type',
        'attachable_id',
        'type',
        'server',
        'path',
        'path_thumb',
        'url',
        'url_thumb',
    ];
    protected $hidden = [];
    protected $dates = [];
    public $timestamps = false;
    protected $casts = [];
    protected $with = [];

    # relationships

    public function attachable()
    {
        return $this->morphTo();
    }
}
