<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 06-Mar-20
 * Time: 18:41
 */

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 *
 * @package App
 * @property int $id
 * @property int $user_id
 * @property string $operation_type
 * @property int $sourceable_id
 * @property string $sourceable_type
 * @property double $amount
 * @property double $balance
 * @property string $desc
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $transactionable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereOperationType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereSourceableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereSourceableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereUserId($value)
 */
class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = [
        'user_id', 'operation_type', 'sourceable_type', 'sourceable_id',
        'amount', 'balance', 'desc'
    ];
    protected $hidden = [];
    protected $dates = [];
    public $timestamps = true;
    protected $casts = ['amount' => 'float', 'balance' => 'float'];
    protected $with = [];

    //

    public function transactionable()
    {
        return $this->morphTo('sourceable');
    }

}
