<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProSkill
 *
 * @package App
 * @property $id
 * @property $user_id
 * @property $skill_id
 * @property $option
 * @property $comp
 * @property-read \App\Skill $skill
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProSkill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProSkill newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProSkill query()
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProSkill whereComp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProSkill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProSkill whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProSkill whereSkillId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ProSkill whereUserId($value)
 */
class ProSkill extends Model
{
    protected $table = 'pro_skill';
    protected $fillable = ['user_id', 'skill_id', 'option', 'comp'];
    protected $hidden = [];
    protected $dates = [];
    public $timestamps = false;
    protected $casts = ['option' => 'boolean'];
    protected $with = [];

}
