<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 06-Mar-20
 * Time: 19:15
 */

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Contract
 *
 * @package App
 * @property int $id
 * @property int $pro_user_id
 * @property int $client_user_id
 * @property array $body
 * @property double $price
 * @property Carbon $due_at
 * @property boolean $was_executed
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @mixin \Eloquent
 * @property-read \App\User $clientUser
 * @property-read \App\User $proUser
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract whereClientUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract whereDueAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract whereProUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract whereWasExecuted($value)
 */
class Contract extends Model
{
    protected $table = 'contracts';
    protected $fillable = [
        'pro_user_id', 'client_user_id', 'body', 'price', 'due_at', 'was_executed'
    ];
    protected $hidden = [];
    protected $dates = ['due_at'];
    public $timestamps = true;
    protected $casts = [
        'was_executed' => 'boolean', 'body' => 'array', 'price' => 'float'
    ];
    protected $with = ['clientUser', 'proUser'];

    # Relationships

    public function clientUser()
    {
        return $this->belongsTo(User::class, 'client_user_id', 'id');
    }

    public function proUser()
    {
        return $this->belongsTo(User::class, 'pro_user_id', 'id');
    }

}
