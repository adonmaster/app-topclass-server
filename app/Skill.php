<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Skill
 *
 * @package App
 * @property $id
 * @property $desc
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Skill whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProSkill[] $proSkill
 * @property-read int|null $pro_skill_count
 */
class Skill extends Model
{
    protected $table = 'skills';
    protected $fillable = ['desc'];
    protected $hidden = [];
    protected $dates = [];
    public $timestamps = true;
    protected $casts = [];
    protected $with = [];


    # Relationships

    public function proSkillAuth() {
        return $this->proSkill()
            ->where('user_id', \Auth::id());
    }

    public function proSkill() {
        return $this->hasMany(ProSkill::class, 'skill_id', 'id');
    }

}
