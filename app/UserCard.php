<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 19-Feb-20
 * Time: 19:47
 */

namespace App;


use Carbon\Carbon;

/**
 * Class UserCard
 *
 * @package App
 * @property $id
 * @property int $user_id
 * @property string $token
 * @property Carbon $expire_at
 * @property Carbon $updated_at
 * @property Carbon $created_at
 */
class UserCard
{
    protected $table = 'user_card';
    protected $fillable = ['user_id', 'token', 'expire_at'];
    protected $hidden = [];
    protected $dates = ['expire_at'];
    public $timestamps = true;
    protected $casts = [];
    protected $with = [];
}