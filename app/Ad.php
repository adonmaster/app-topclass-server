<?php
/**
 * Created by PhpStorm.
 * User: Adon
 * Date: 08-Apr-20
 * Time: 19:55
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ad
 *
 * @package App
 * @property int $id
 * @property int $client_user_id
 * @property array $body
 * @property Carbon $expire_at
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @mixin \Eloquent
 * @property-read \App\User $clientUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\AdPro[] $pros
 * @property-read int|null $pros_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad whereClientUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad whereExpireAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Ad whereUpdatedAt($value)
 */
class Ad extends Model
{
    protected $table = 'ads';
    protected $fillable = [
        'client_user_id', 'body', 'expire_at'
    ];
    protected $hidden = [];
    protected $dates = ['expire_at'];
    public $timestamps = true;
    protected $casts = [
        'body' => 'array'
    ];
    protected $with = ['clientUser', 'pros'];


    # misc

    /**
     * @return bool
     */
    public function isExpired()
    {
        return $this->expire_at->isPast();
    }

    # scope

    public function scopeNotExpired($q)
    {
        return $q->where('expire_at', '>=', Carbon::now());
    }


    # Relationships

    public function clientUser()
    {
        return $this->belongsTo(User::class, 'client_user_id', 'id');
    }

    public function pros()
    {
        return $this->hasMany(AdPro::class, 'ad_id', 'id');
    }


}