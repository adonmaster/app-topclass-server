@extends('template')

@section('content')

    <style>
        html,
        body, .body {
            height: 100%;
        }

        .body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }
        .form-signin .checkbox {
            font-weight: 400;
        }
        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>

    <div class="body">

        <form class="form-signin " action="{{ route('login') }}" method="POST">

            <div class="text-center">
                <img class="mb-4" src="{{asset('imgs/logo.png')}}" alt="" width="72">
            </div>

            {{--flash--}}
            <div class="col-12">
                {!! \App\Adon\Flash\Flash::render() !!}
            </div>

            {!! csrf_field() !!}

            <h1 class="h3 mb-3 font-weight-normal">Login</h1>

            @component('components.bs.input-text', [
                        'label' => 'Email',
                        'field' => 'email', 'type' => 'email', 'ph' => 'Insira seu email aqui...'
                    ])
            @endcomponent

            @component('components.bs.input-text', [
                'label' => 'Senha',
                'field' => 'password', 'type' => 'password', 'ph' => '******'
            ])
            @endcomponent

            <button class="btn btn-lg btn-primary btn-block" type="submit">ENTRAR</button>
        </form>
    </div>



@endsection