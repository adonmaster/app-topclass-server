{{--

Input-Text Component
fields:
    *field
    label=null
    ph=null
--}}

@php
    $label = isset($label) ? $label : null;
    $rows = isset($rows) ? $rows : 3;
    $ph = isset($ph) ? $ph : null;

    $old = old($field);
    $pristine = !$old && !$errors->has($field);
    $validationClass = $pristine ? '' : ($errors->has($field) ? 'is-invalid' : 'is-valid')
@endphp

<div class="form-group">

    @if($label)
        <label for="{{ $field }}">{{ $label }}</label>
    @endif

    <textarea class="form-control {{ $validationClass }}"
        id="{{ $field }}"
        name="{{ $field }}"
        rows="{{  $rows }}"
        placeholder="{{ $ph }}"
        >{{$old}}</textarea>

    <div class="invalid-feedback">
        {{ $errors->first($field) }}
    </div>

</div>
