@extends('mails.template')

@section('content-title')
    Olá!
@endsection


@section('content-subtitle')

@endsection


@section('content-body')

    <p style="text-align: center">
        Seu código de verificação é:
        <br>
        <br>
        <strong style="font-size: 16pt">{{ $code }}</strong>
    </p>

@endsection