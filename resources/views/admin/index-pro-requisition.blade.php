@extends('admin.template')

@section('content')

    @include('admin._nav-header')

    <div class="container">
        {!! \App\Adon\Flash\Flash::render() !!}
    </div>

    <div class="container-fluid">

        {{--count--}}
        <div class="text-center">
            <span class="badge badge-light">
                {{count($requisitions)}} item(s)
            </span>
        </div>

        {{--list--}}
        <ul class="list-unstyled">
            @foreach($requisitions as $requisition)
                <li class="media mb-4">

                    <img src="{{$requisition->user->p()->avatarThumb()}}"
                         style="object-fit: cover"
                         class="mr-3 rounded-circle" alt="Avatar" width="60px" height="60px"
                        >

                    <div class="media-body">

                        <h4 class="mt-0 mb-1 fw800">
                            <small class="mdc-text-grey-300">#{{$requisition->id}}</small>
                            {{$requisition->user->name}}
                            <small>({{$requisition->user->email}})</small>
                        </h4>

                        {{nl2br($requisition->obs)}}

                        <div class="mt-1">

                            @if(count($requisition->attachments))
                                <a href="{{$requisition->attachments[0]->url}}" class="btn btn-secondary"
                                   target="_blank">
                                    <i class="fa fa-file-pdf-o fa-fw"></i>
                                    Currículo
                                </a>
                            @endif

                            &nbsp;&nbsp;&nbsp;

                            {{--remove--}}
                            {!! Form::open(['method'=>'delete', 'route'=>'admin.index-pro-requisition', 'class'=>'d-inline-block']) !!}
                                {!! Form::hidden('id', $requisition->id) !!}
                                <button type="submit" class="btn btn-outline-danger">
                                    <i class="fa fa-trash fa-fw"></i>
                                    Remover
                                </button>
                            {!! Form::close() !!}

                            {{--check--}}
                            {!! Form::open(['method'=>'post', 'route'=>'admin.index-pro-requisition', 'class'=>'d-inline-block']) !!}
                                {!! Form::hidden('id', $requisition->id) !!}
                                <button type="submit" class="btn btn-outline-success">
                                    <i class="fa fa-check fa-fw"></i>
                                    Habilitar
                                </button>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
        {{--list--}}

    </div>

@endsection