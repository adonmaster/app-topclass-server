<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h5 class="my-0 mr-md-auto font-weight-normal">TopClass - Admin</h5>

    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="{{ route('admin.index-pro-requisition') }}">
            <i class="zmdi zmdi-graduation-cap"></i>
            Requisição de Professores
        </a>
    </nav>

    <form action="{{route('login')}}" method="DELETE" style="display: inline-block">
        <button type="submit" class="btn btn-outline-danger">
            Sair
        </button>
    </form>
</div>